const express = require('express');

const request = require('request-promise-native')


module.exports = async (params) => {
  const router = express.Router();
  /* GET home page. */
  router.get('/', async function(req, res, next) {
    res.render('index', { title: 'Chan', boards:params.boards });
  });


  return router;
}
