const express = require('express');
const request = require('request-promise-native')

const PAGES = 10;
const NUM_OF_COMMENTS_TO_SHOW = 3;


const compute_extra_board_data = (board, api_url) => {
  return Object.assign(board, {
    threads:board.threads.map(thread => {
      return compute_extra_thread_data(board, thread, api_url)
    })
  })
}
const compute_extra_thread_data = (board, thread, api_url) => {
  const threadsPerPage = board.totalThreadCount / PAGES;
  return Object.assign(thread, {
    page: (thread.idx / threadsPerPage) + 1,
    totalPosts:thread.posts.length,
    totalImages:thread.posts.filter(p => p.media).length,
    posts:thread.posts.map(p => {
      if (!p.media)
        return p;
      return Object.assign(p,{
        media: Object.assign(p.media, {
          url: `${api_url}${p.media.url}`,
        })
      })
    }),
    lastPostsArr:
      Array.from(Array(
        Math.min(NUM_OF_COMMENTS_TO_SHOW, thread.posts.length-1)
      ).keys()).map((_,i,a)=>thread.posts.length - a.length + i)
  });
}

module.exports = async (params) => {
  const router = express.Router();
  const board = params.board;


  /* GET home page. */
  router.get('/', async function(req, res, next) {
    const api_url = `${params.api_url}${board.short}`;

    let board_data = await request.get(
      api_url,
      {json:true}
    );
    board_data = compute_extra_board_data(board_data, params.api_url);

    res.render('board', {board:board_data });
  });



  router.post(`/post`, async function(req, res, next){
    res.render('index');
  });


  router.get(`/:threadNo/`, async function(req, res, next){
    console.log('thread')
    const api_url = `${params.api_url}${board.short}/${req.params.threadNo}`;

    let thread_data = await request.get(
      api_url,
      {json:true}
    );
    thread_data = compute_extra_thread_data(board, thread_data, params.api_url);
    console.log(thread_data.posts);
    console.log(thread_data.posts[2]);
    res.render('thread', {board:board, thread:thread_data });
  });


  return router;
}
