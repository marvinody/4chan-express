const express = require('express');
const request = require('request-promise-native')

module.exports = async (params) => {
  const router = express.Router();
  const board = params.board;
  /* GET home page. */
  console.log(board);
  router.get('/', async function(req, res, next) {
    const api_url = `${params.api_url}${board.short}`;

    let board_data = await request.get(
      api_url,
      {json:true}
    );
    board_data = compute_extra_data(board_data, params.api_url);

    res.render('board', {board:board_data });
  });

  router.post(`/post`, async function(req, res, next){
    res.render('index');
  });


  return router;
}
